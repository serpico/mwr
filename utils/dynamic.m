function [out] = dynamic(M)

minV = min(M(:));
maxV = max(M(:));

out = [minV maxV];